﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VDirectAccessWrapper;
using VDirectAccessWrapper.mmsxVDirectAccess;

namespace VDirectAccessWraperDemo
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private int _idMerchant;

        public int IdMerchant
        {
            get { return _idMerchant; }
            set
            {
                AddingMerchant.Text = value.ToString();
                _idMerchant = value;
            }
        }

        private int _userId=-1;

        private int _idLocation;

        public int IdLocations
        {
            get { return _idLocation; }
            set
            {
                AddingLocation.Text = value.ToString();
                _idLocation = value;
            }
        }

        private WrapperVDirectAccess VDirect { get; set; }
        private TerminalParams CurentParams { get; set; }

        public int IdUser
        {
            get { return _userId; }
            set
            {
                UserIdLabel.Text = value.ToString();
                _userId = value;
            }
        }

        public DvmmsObjects CurentObjects  =new DvmmsObjects();
        private bool? _flagAction=null;
        public System.Data.DataSet MerchantAndLocations= null;

        private void Login_Click(object sender, EventArgs e)
        {
            if ((!string.IsNullOrEmpty(UserName.Text)) && (!string.IsNullOrEmpty(Password.Text)))
            {
                VDirect = new WrapperVDirectAccess(UserName.Text, Password.Text);
                var ds = VDirect.ShowTemplateList();
                TemplateDataGrid.DataSource = ds.Tables[0];
                var str = ds.Tables[0].Columns.Cast<DataColumn>().Aggregate("", (current, column) => current + (column.ColumnName + "\t" + column.DataType + "\t" + column.DefaultValue + "\n"));
                MerchantAndLocations =  VDirect.MerchantLocationList();

                Debug.WriteLine(str);
            }
            else
            {
                MessageBox.Show("user password");
            }


        }

        private void CreateTerminal_Click(object sender, EventArgs e)
        {
            CurentParams = new TerminalParams();
            CurentParams.CurentTerminal = new Terminal();
            CurentParams.CurentTerminal.DownloadId = TerminalDownloadId.Text;
            CurentParams.CurentTerminal.TemplateId = int.Parse(TerminalTemplateId.Text);
            CurentParams.CurentTerminal.Description = TerminalDescription.Text;
            CurentParams.CurentTerminal.OptionalInfo = TerminalOptionalInfo.Text;
            CurentParams.CurentTerminal.TotalMerchants = int.Parse(TeminalTotalMerchants.Text);
            var flag=VDirect.InsertEmptyTerminal(CurentParams.CurentTerminal);
            if (flag)
            {
                MessageBox.Show(@"Insert terminal ok");
               var ds = VDirect.ShowTemplateParams(CurentParams.CurentTerminal);
                CurentParams.Params = ds;
                DataGridCurrentParameters.DataSource = ds.Tables[0];
                var str = ds.Tables[0].Columns.Cast<DataColumn>().Aggregate("", (current, column) => current + (column.ColumnName + "\t" + column.DataType + "\t" + column.DefaultValue + "\n"));
                Debug.WriteLine(str);
            }

        }

        private void UpdateParameters_Click(object sender, EventArgs e)
        {
           var flag= VDirect.InsertTerminalParams(CurentParams);
            if (flag)
            {
                MessageBox.Show(@"Insert terminal params ok");
            }
            VDirect.PushToDataCenter(CurentParams.CurentTerminal, DOWNLOADTYPE.INSERT);
        }

        private void GetCompanies_Click(object sender, EventArgs e)
        {
            var ds = VDirect.GetCompany();
            CurentObjects.Companies = ds;
            _flagAction = false;
            DataGridCurrentParameters.DataSource = ds.Tables[0];
            var str = ds.Tables[0].Columns.Cast<DataColumn>().Aggregate("", (current, column) => current + (column.ColumnName + "\t" + column.DataType + "\t" + column.DefaultValue + "\n"));
            Debug.WriteLine(str);
        }

        private void GetTerminals_Click(string companyId)
        {
            var company = 0;
            if (int.TryParse(companyId, out company))
            {
                var ds = VDirect.GetTerminals(company);
                CurentObjects.Terminals = ds;
                _flagAction = true;
                DataGridCurrentParameters.DataSource = ds.Tables[0];
                var str = ds.Tables[0].Columns.Cast<DataColumn>().Aggregate("", (current, column) => current + (column.ColumnName + "\t" + column.DataType + "\t" + column.DefaultValue + "\n"));
                Debug.WriteLine(str);
            }
        }

        private void GetTerminlParams_Click(object sender, EventArgs e)
        {
            CurentParams = new TerminalParams();
            CurentParams.CurentTerminal = new Terminal();
            CurentParams.CurentTerminal.DownloadId = TerminalDownloadId.Text;
            CurentParams.CurentTerminal.TemplateId = int.Parse(TerminalTemplateId.Text);
            CurentParams.CurentTerminal.Description = TerminalDescription.Text;
            CurentParams.CurentTerminal.OptionalInfo = TerminalOptionalInfo.Text;
            CurentParams.CurentTerminal.TotalMerchants = int.Parse(TeminalTotalMerchants.Text);

            var ds = VDirect.GetTerminalParams(CurentParams.CurentTerminal);
            CurentParams.Params = ds;
            DataGridCurrentParameters.DataSource = ds.Tables[0];
            var str = ds.Tables[0].Columns.Cast<DataColumn>().Aggregate("", (current, column) => current + (column.ColumnName + "\t" + column.DataType + "\t" + column.DefaultValue + "\n"));
            Debug.WriteLine(str);
        }

        private void TemplateDataGrid_DoubleClick(object sender, EventArgs e)
        {
            
        }

        private void TemplateDataGrid_SelectionChanged(object sender, EventArgs e)
        {
            var selectedColumnCount = TemplateDataGrid.Rows
                .GetRowCount(DataGridViewElementStates.Selected);
            if (selectedColumnCount > 0)
            {
                for (var i = 0; i < selectedColumnCount; i++)
                {
                    var selectrows = TemplateDataGrid.SelectedRows[i];
                    TerminalTemplateId.Text = selectrows.Cells["Template_ID"].Value.ToString();
                }

            }
        }

        private void DataGridCurrentParameters_SelectionChanged(object sender, EventArgs e)
        {
            var selectedColumnCount = DataGridCurrentParameters.Rows
                .GetRowCount(DataGridViewElementStates.Selected);
            if (selectedColumnCount > 0)
            {
                for (var i = 0; i < selectedColumnCount; i++)
                {
                    var selectrows = DataGridCurrentParameters.SelectedRows[i];
                    if(_flagAction!=null)
                    {
                        if (_flagAction.Value)
                            TerminalDownloadId.Text = selectrows.Cells["tpn"].Value.ToString();
                        else
                        {
                            GetTerminals_Click(selectrows.Cells["Company_ID"].Value.ToString());
                        }
                    }
                    else
                    {
                    }
                }

            }
        }

        private void UpdateParametr_Click(object sender, EventArgs e)
        {
           
            //if (selectedColumnCount > 0)
            //{
            //    for (var i = 0; i < selectedColumnCount; i++)
            //    {
            //        var selectrows = TemplateDataGrid.SelectedRows[i];
            //        TerminalTemplateId.Text = selectrows.Cells["Template_ID"].Value.ToString();
            //    }

            //}
        }

        private DataGridViewRow dgvr = null;

        private void DataGridCurrentParameters_CellBeginEdit(object sender, DataGridViewCellCancelEventArgs e)
        {

            var selectedColumnCount = TemplateDataGrid.Rows
                .GetRowCount(DataGridViewElementStates.Selected);
            if (selectedColumnCount ==1)
            {
                for (var i = 0; i < selectedColumnCount; i++)
                {
                    dgvr = TemplateDataGrid.SelectedRows[i];
                }
            }
        }

        private void DataGridCurrentParameters_CellEndEdit(object sender, DataGridViewCellEventArgs e)
        {
            //var selectedColumnCount = TemplateDataGrid.Rows
            //   .GetRowCount(DataGridViewElementStates.Selected);
            //if (selectedColumnCount == 1)
            //{
            //    for (var i = 0; i < selectedColumnCount; i++)
            //    {
            //        var currentdgvr = TemplateDataGrid.SelectedRows[i];
            //        foreach (var cell in currentdgvr.Cells)
            //        {
            //            dgvr.Cells[cell.]
            //        }
            //    }
            //}
        }

        private void AddMerchant_Click(object sender, EventArgs e)
        {
           var al=new AddMerchant(UserName.Text,Password.Text,this);
            al.ShowDialog();
        }

        private void AddLocations_Click(object sender, EventArgs e)
        {
            var al = new AddLocations(UserName.Text, Password.Text,IdMerchant,this);
            al.ShowDialog();
        }

        private void AssignLocationToTpn_Click(object sender, EventArgs e)
        {
            var flag= VDirect.LocationAssignTpn(CurentParams.CurentTerminal.DownloadId, IdLocations);
            MessageBox.Show(flag ? "ok" : "False");
        }

        private void AddUser_Click(object sender, EventArgs e)
        {
            var al = new AddUser(UserName.Text, Password.Text, this);
            al.ShowDialog();
        }
    }

    public class DvmmsObjects
    {
        public int CurrentCompany { get; set; }
        public DataSet Companies { get; set; }
        public DataSet Terminals { get; set; }
    }
}
