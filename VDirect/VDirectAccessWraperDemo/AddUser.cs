﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using VDirectAccessWrapper;

namespace VDirectAccessWraperDemo
{
    public partial class AddUser : Form
    {
        private readonly string _user;
        private readonly string _password;
        private readonly Form1 _mainForm;

        public AddUser(string user, string password, Form1 mainForm)
        {
            _user = user;
            _password = password;
            _mainForm = mainForm;
            InitializeComponent();
        }

        private void SaveUser_Click(object sender, EventArgs e)
        {
            var user = new VDirectAccessWrapper.mmsxVDirectAccess.MerchantUser();
            user.CellPhone = UserCPhone.Text;
            user.Email = UserEmail.Text;
            user.FullName = UserFName.Text;
            user.LoginName = UserLName.Text;
            user.Phone = UserPhone.Text;
            var vdc = new WrapperVDirectAccess(_user, _password);
            const string outstr = "";
            var result = vdc.AddUser(user);
            MessageBox.Show(string.IsNullOrEmpty(outstr) ? $"Return {result}" : $"Return {outstr}");
            _mainForm.IdUser = result;
            Close();
        }
    }
}
