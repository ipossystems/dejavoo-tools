﻿using System;
using System.Data;
using System.Windows.Forms;
using VDirectAccessWrapper;
using VDirectAccessWrapper.mmsxVDirectAccess;

namespace VDirectAccessWraperDemo
{
    public partial class AddMerchant : Form
    {
        private readonly string _user;
        private readonly string _password;
        private readonly Form1 _mainForm;
        public string NameField { get; set; }

        public AddMerchant(string user,string password,Form1 mainForm)
        {
            _user = user;
            _password = password;
            _mainForm = mainForm;
            InitializeComponent();
            if (_mainForm.MerchantAndLocations != null)
            {
                var merchs = _mainForm.MerchantAndLocations.Tables["Merchants"].DataSet;
                dataGridMerchant.DataSource = merchs.Tables[0];
            }
        }

        private void MerchantAdd_Click(object sender, EventArgs e)
        {
            var vdc=new WrapperVDirectAccess(_user, _password);
            var outstr = "";
            var result= vdc.MerchantAdd(MerchantName.Text,_mainForm.IdUser);
            MessageBox.Show(string.IsNullOrEmpty(outstr) ? $"Return {result}" : $"Return {outstr}");
            _mainForm.IdMerchant = result;
            Close();
        }

        private void dataGridMerchant_SelectionChanged(object sender, EventArgs e)
        {
            var selectedColumnCount = dataGridMerchant.Rows
                .GetRowCount(DataGridViewElementStates.Selected);
            if (selectedColumnCount > 0)
            {
                for (var i = 0; i < selectedColumnCount; i++)
                {
                    var selectrows = dataGridMerchant.SelectedRows[i];

                    _mainForm.IdMerchant = int.Parse(selectrows.Cells["Id"].Value.ToString());
                    Close();
                }
            }
        }
    }
}
