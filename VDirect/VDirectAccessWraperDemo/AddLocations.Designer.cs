﻿namespace VDirectAccessWraperDemo
{
    partial class AddLocations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SaveLocations = new System.Windows.Forms.Button();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.ShippingEmail = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.ShippingPhone = new System.Windows.Forms.TextBox();
            this.label23 = new System.Windows.Forms.Label();
            this.ShippingPostalCode = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.ShippingCountry = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.ShippingState = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.ShippingCity = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.ShippingAddress2 = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.ShippingAddress = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.ShippingCompanyName = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.ShippingLastName = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.ShippingFirstName = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.BillingEmail = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.BillingPhone = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.BillingPostalCode = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.BillingState = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.BillingCity = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.BillingAddress2 = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.BillingAddress = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.BillingCompanyName = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.BillingLastName = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.BillingFirstName = new System.Windows.Forms.TextBox();
            this.MerchantDescription = new System.Windows.Forms.TextBox();
            this.MerchantName = new System.Windows.Forms.TextBox();
            this.BillingCountry = new System.Windows.Forms.TextBox();
            this.Priority = new System.Windows.Forms.NumericUpDown();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.dataGridLocation = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.Priority)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridLocation)).BeginInit();
            this.SuspendLayout();
            // 
            // SaveLocations
            // 
            this.SaveLocations.Location = new System.Drawing.Point(388, 655);
            this.SaveLocations.Name = "SaveLocations";
            this.SaveLocations.Size = new System.Drawing.Size(121, 23);
            this.SaveLocations.TabIndex = 106;
            this.SaveLocations.Text = "Save Locations";
            this.SaveLocations.UseVisualStyleBackColor = true;
            this.SaveLocations.Click += new System.EventHandler(this.SaveLocations_Click_1);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Location = new System.Drawing.Point(471, 382);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(38, 13);
            this.label25.TabIndex = 103;
            this.label25.Text = "Priority";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Location = new System.Drawing.Point(471, 338);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(73, 13);
            this.label26.TabIndex = 101;
            this.label26.Text = "ShippingEmail";
            // 
            // ShippingEmail
            // 
            this.ShippingEmail.Location = new System.Drawing.Point(474, 354);
            this.ShippingEmail.Name = "ShippingEmail";
            this.ShippingEmail.Size = new System.Drawing.Size(200, 20);
            this.ShippingEmail.TabIndex = 100;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(471, 304);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(79, 13);
            this.label22.TabIndex = 99;
            this.label22.Text = "ShippingPhone";
            // 
            // ShippingPhone
            // 
            this.ShippingPhone.Location = new System.Drawing.Point(474, 320);
            this.ShippingPhone.Name = "ShippingPhone";
            this.ShippingPhone.Size = new System.Drawing.Size(200, 20);
            this.ShippingPhone.TabIndex = 98;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Location = new System.Drawing.Point(471, 263);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(102, 13);
            this.label23.TabIndex = 97;
            this.label23.Text = "ShippingPostalCode";
            // 
            // ShippingPostalCode
            // 
            this.ShippingPostalCode.Location = new System.Drawing.Point(474, 279);
            this.ShippingPostalCode.Name = "ShippingPostalCode";
            this.ShippingPostalCode.Size = new System.Drawing.Size(200, 20);
            this.ShippingPostalCode.TabIndex = 96;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(471, 219);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(84, 13);
            this.label20.TabIndex = 95;
            this.label20.Text = "ShippingCountry";
            // 
            // ShippingCountry
            // 
            this.ShippingCountry.Location = new System.Drawing.Point(474, 235);
            this.ShippingCountry.Name = "ShippingCountry";
            this.ShippingCountry.Size = new System.Drawing.Size(200, 20);
            this.ShippingCountry.TabIndex = 94;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(471, 178);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(73, 13);
            this.label21.TabIndex = 93;
            this.label21.Text = "ShippingState";
            // 
            // ShippingState
            // 
            this.ShippingState.Location = new System.Drawing.Point(474, 194);
            this.ShippingState.Name = "ShippingState";
            this.ShippingState.Size = new System.Drawing.Size(200, 20);
            this.ShippingState.TabIndex = 92;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(471, 134);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(65, 13);
            this.label18.TabIndex = 91;
            this.label18.Text = "ShippingCity";
            // 
            // ShippingCity
            // 
            this.ShippingCity.Location = new System.Drawing.Point(474, 150);
            this.ShippingCity.Name = "ShippingCity";
            this.ShippingCity.Size = new System.Drawing.Size(200, 20);
            this.ShippingCity.TabIndex = 90;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(471, 93);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(92, 13);
            this.label19.TabIndex = 89;
            this.label19.Text = "ShippingAddress2";
            // 
            // ShippingAddress2
            // 
            this.ShippingAddress2.Location = new System.Drawing.Point(474, 109);
            this.ShippingAddress2.Name = "ShippingAddress2";
            this.ShippingAddress2.Size = new System.Drawing.Size(200, 20);
            this.ShippingAddress2.TabIndex = 88;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(471, 47);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(86, 13);
            this.label16.TabIndex = 87;
            this.label16.Text = "ShippingAddress";
            // 
            // ShippingAddress
            // 
            this.ShippingAddress.Location = new System.Drawing.Point(474, 63);
            this.ShippingAddress.Name = "ShippingAddress";
            this.ShippingAddress.Size = new System.Drawing.Size(200, 20);
            this.ShippingAddress.TabIndex = 86;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(471, 6);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(120, 13);
            this.label17.TabIndex = 85;
            this.label17.Text = "ShippingCompanyName";
            // 
            // ShippingCompanyName
            // 
            this.ShippingCompanyName.Location = new System.Drawing.Point(474, 22);
            this.ShippingCompanyName.Name = "ShippingCompanyName";
            this.ShippingCompanyName.Size = new System.Drawing.Size(200, 20);
            this.ShippingCompanyName.TabIndex = 84;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(210, 587);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(96, 13);
            this.label15.TabIndex = 83;
            this.label15.Text = "ShippingLastName";
            // 
            // ShippingLastName
            // 
            this.ShippingLastName.Location = new System.Drawing.Point(213, 603);
            this.ShippingLastName.Name = "ShippingLastName";
            this.ShippingLastName.Size = new System.Drawing.Size(200, 20);
            this.ShippingLastName.TabIndex = 82;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(210, 546);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(95, 13);
            this.label14.TabIndex = 81;
            this.label14.Text = "ShippingFirstName";
            // 
            // ShippingFirstName
            // 
            this.ShippingFirstName.Location = new System.Drawing.Point(213, 562);
            this.ShippingFirstName.Name = "ShippingFirstName";
            this.ShippingFirstName.Size = new System.Drawing.Size(200, 20);
            this.ShippingFirstName.TabIndex = 80;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(210, 503);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(59, 13);
            this.label13.TabIndex = 79;
            this.label13.Text = "BillingEmail";
            // 
            // BillingEmail
            // 
            this.BillingEmail.Location = new System.Drawing.Point(213, 519);
            this.BillingEmail.Name = "BillingEmail";
            this.BillingEmail.Size = new System.Drawing.Size(200, 20);
            this.BillingEmail.TabIndex = 78;
            this.BillingEmail.Text = "kirillk@dejavoosystems.ru";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(210, 462);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(65, 13);
            this.label12.TabIndex = 77;
            this.label12.Text = "BillingPhone";
            // 
            // BillingPhone
            // 
            this.BillingPhone.Location = new System.Drawing.Point(213, 478);
            this.BillingPhone.Name = "BillingPhone";
            this.BillingPhone.Size = new System.Drawing.Size(200, 20);
            this.BillingPhone.TabIndex = 76;
            this.BillingPhone.Text = "(123) 423-4123";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(210, 423);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(88, 13);
            this.label11.TabIndex = 75;
            this.label11.Text = "BillingPostalCode";
            // 
            // BillingPostalCode
            // 
            this.BillingPostalCode.Location = new System.Drawing.Point(213, 439);
            this.BillingPostalCode.Name = "BillingPostalCode";
            this.BillingPostalCode.Size = new System.Drawing.Size(200, 20);
            this.BillingPostalCode.TabIndex = 74;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(210, 383);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(59, 13);
            this.label10.TabIndex = 73;
            this.label10.Text = "BillingState";
            // 
            // BillingState
            // 
            this.BillingState.Location = new System.Drawing.Point(213, 399);
            this.BillingState.Name = "BillingState";
            this.BillingState.Size = new System.Drawing.Size(200, 20);
            this.BillingState.TabIndex = 72;
            this.BillingState.Text = "AK";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(210, 341);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(70, 13);
            this.label9.TabIndex = 71;
            this.label9.Text = "BillingCountry";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(210, 302);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(51, 13);
            this.label8.TabIndex = 69;
            this.label8.Text = "BillingCity";
            // 
            // BillingCity
            // 
            this.BillingCity.Location = new System.Drawing.Point(213, 318);
            this.BillingCity.Name = "BillingCity";
            this.BillingCity.Size = new System.Drawing.Size(200, 20);
            this.BillingCity.TabIndex = 68;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(210, 263);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(78, 13);
            this.label7.TabIndex = 67;
            this.label7.Text = "BillingAddress2";
            // 
            // BillingAddress2
            // 
            this.BillingAddress2.Location = new System.Drawing.Point(213, 279);
            this.BillingAddress2.Name = "BillingAddress2";
            this.BillingAddress2.Size = new System.Drawing.Size(200, 20);
            this.BillingAddress2.TabIndex = 66;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(210, 219);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(72, 13);
            this.label6.TabIndex = 65;
            this.label6.Text = "BillingAddress";
            // 
            // BillingAddress
            // 
            this.BillingAddress.Location = new System.Drawing.Point(213, 235);
            this.BillingAddress.Name = "BillingAddress";
            this.BillingAddress.Size = new System.Drawing.Size(200, 20);
            this.BillingAddress.TabIndex = 64;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(210, 178);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(106, 13);
            this.label5.TabIndex = 63;
            this.label5.Text = "BillingCompanyName";
            // 
            // BillingCompanyName
            // 
            this.BillingCompanyName.Location = new System.Drawing.Point(213, 194);
            this.BillingCompanyName.Name = "BillingCompanyName";
            this.BillingCompanyName.Size = new System.Drawing.Size(200, 20);
            this.BillingCompanyName.TabIndex = 62;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(210, 134);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(82, 13);
            this.label4.TabIndex = 61;
            this.label4.Text = "BillingLastName";
            // 
            // BillingLastName
            // 
            this.BillingLastName.Location = new System.Drawing.Point(213, 150);
            this.BillingLastName.Name = "BillingLastName";
            this.BillingLastName.Size = new System.Drawing.Size(200, 20);
            this.BillingLastName.TabIndex = 60;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(210, 93);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 13);
            this.label3.TabIndex = 59;
            this.label3.Text = "BillingFirstName";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(210, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 13);
            this.label2.TabIndex = 58;
            this.label2.Text = "Description";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(210, 6);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(35, 13);
            this.label1.TabIndex = 57;
            this.label1.Text = "Name";
            // 
            // BillingFirstName
            // 
            this.BillingFirstName.Location = new System.Drawing.Point(213, 109);
            this.BillingFirstName.Name = "BillingFirstName";
            this.BillingFirstName.Size = new System.Drawing.Size(200, 20);
            this.BillingFirstName.TabIndex = 56;
            // 
            // MerchantDescription
            // 
            this.MerchantDescription.Location = new System.Drawing.Point(213, 66);
            this.MerchantDescription.Name = "MerchantDescription";
            this.MerchantDescription.Size = new System.Drawing.Size(200, 20);
            this.MerchantDescription.TabIndex = 55;
            // 
            // MerchantName
            // 
            this.MerchantName.Location = new System.Drawing.Point(213, 22);
            this.MerchantName.Name = "MerchantName";
            this.MerchantName.Size = new System.Drawing.Size(200, 20);
            this.MerchantName.TabIndex = 54;
            // 
            // BillingCountry
            // 
            this.BillingCountry.Location = new System.Drawing.Point(213, 360);
            this.BillingCountry.Name = "BillingCountry";
            this.BillingCountry.Size = new System.Drawing.Size(200, 20);
            this.BillingCountry.TabIndex = 107;
            this.BillingCountry.Text = "USA";
            // 
            // Priority
            // 
            this.Priority.Location = new System.Drawing.Point(474, 400);
            this.Priority.Name = "Priority";
            this.Priority.Size = new System.Drawing.Size(120, 20);
            this.Priority.TabIndex = 108;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(475, 439);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(98, 17);
            this.checkBox1.TabIndex = 109;
            this.checkBox1.Text = "IsMainLocation";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // dataGridLocation
            // 
            this.dataGridLocation.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridLocation.Location = new System.Drawing.Point(750, 22);
            this.dataGridLocation.Name = "dataGridLocation";
            this.dataGridLocation.Size = new System.Drawing.Size(389, 601);
            this.dataGridLocation.TabIndex = 110;
            this.dataGridLocation.SelectionChanged += new System.EventHandler(this.dataGridLocation_SelectionChanged);
            // 
            // AddLocations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1163, 684);
            this.Controls.Add(this.dataGridLocation);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.Priority);
            this.Controls.Add(this.BillingCountry);
            this.Controls.Add(this.SaveLocations);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.label26);
            this.Controls.Add(this.ShippingEmail);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.ShippingPhone);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.ShippingPostalCode);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.ShippingCountry);
            this.Controls.Add(this.label21);
            this.Controls.Add(this.ShippingState);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.ShippingCity);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.ShippingAddress2);
            this.Controls.Add(this.label16);
            this.Controls.Add(this.ShippingAddress);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.ShippingCompanyName);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.ShippingLastName);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.ShippingFirstName);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.BillingEmail);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.BillingPhone);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.BillingPostalCode);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.BillingState);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.BillingCity);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.BillingAddress2);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.BillingAddress);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.BillingCompanyName);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.BillingLastName);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BillingFirstName);
            this.Controls.Add(this.MerchantDescription);
            this.Controls.Add(this.MerchantName);
            this.Name = "AddLocations";
            this.Text = "Add Locations";
            this.Load += new System.EventHandler(this.AddLocations_Load);
            ((System.ComponentModel.ISupportInitialize)(this.Priority)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridLocation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button SaveLocations;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TextBox ShippingEmail;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox ShippingPhone;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.TextBox ShippingPostalCode;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox ShippingCountry;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox ShippingState;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox ShippingCity;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox ShippingAddress2;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox ShippingAddress;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox ShippingCompanyName;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox ShippingLastName;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox ShippingFirstName;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox BillingEmail;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox BillingPhone;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox BillingPostalCode;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox BillingState;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox BillingCity;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox BillingAddress2;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox BillingAddress;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox BillingCompanyName;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox BillingLastName;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox BillingFirstName;
        private System.Windows.Forms.TextBox MerchantDescription;
        private System.Windows.Forms.TextBox MerchantName;
        private System.Windows.Forms.TextBox BillingCountry;
        private System.Windows.Forms.NumericUpDown Priority;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.DataGridView dataGridLocation;
    }
}