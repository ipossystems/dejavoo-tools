﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
//using ServiceApp.Entity.modelauto;
using VDirectAccessWrapper;
using VDirectAccessWrapper.mmsxVDirectAccess;

namespace VDirectAccessWraperDemo
{
    public partial class AddLocations : Form
    {
        private readonly string _user;
        private readonly string _password;
        private readonly int _idMerchant;
        private readonly Form1 _form1;

        public AddLocations(string user, string password, int idMerchant, Form1 form1)
        {
            _user = user;
            _password = password;
            _idMerchant = idMerchant;
            _form1 = form1;
            InitializeComponent();
            if (_form1.MerchantAndLocations != null)
            {
                var merchs = _form1.MerchantAndLocations.Tables["Locations"].DataSet;
                dataGridLocation.DataSource = merchs.Tables[1];
            }
        }

        private void SaveLocations_Click_1(object sender, EventArgs e)
        {
            var lc = new Location();
            lc.IsMainLocation = checkBox1.Checked;
            lc.ShippingEmail = ShippingEmail.Text;
            lc.ShippingPhone = ShippingPhone.Text;
            lc.ShippingPostalCode = ShippingPostalCode.Text;
            lc.ShippingCountry = ShippingCountry.Text;
            lc.ShippingState = ShippingState.Text;
            lc.ShippingCity = ShippingCity.Text;
            lc.ShippingAddress2 = ShippingAddress2.Text;
            lc.ShippingAddress = ShippingAddress.Text;
            lc.ShippingCompanyName = ShippingCompanyName.Text;
            lc.ShippingLastName = ShippingLastName.Text;
            lc.ShippingFirstName = ShippingFirstName.Text;
            lc.BillingEmail = BillingEmail.Text;
            lc.BillingPhone = BillingPhone.Text;
            lc.BillingPostalCode = BillingPostalCode.Text;
            lc.BillingState = BillingState.Text;
            lc.BillingCity = BillingCity.Text;
            lc.BillingAddress2 = BillingAddress2.Text;
            lc.BillingAddress = BillingAddress.Text;
            lc.BillingCompanyName = BillingCompanyName.Text;
            lc.BillingLastName = BillingLastName.Text;
            lc.BillingFirstName = BillingFirstName.Text;
            lc.Description = MerchantDescription.Text;
            lc.Name = MerchantName.Text;
            lc.BillingCountry = BillingCountry.Text;
            lc.Priority = (PriorityEnum) Priority.Value;
            lc.MerchantId = _idMerchant;

            var vdc = new WrapperVDirectAccess(_user, _password);
            const string outstr = "";
            var result = vdc.LocationAdd( lc);
            MessageBox.Show(string.IsNullOrEmpty(outstr) ? $"Return {result}" : $"Return {outstr}");
            _form1.IdLocations = result;
            Close();
        }

        private void AddLocations_Load(object sender, EventArgs e)
        {

        }

        private void dataGridLocation_SelectionChanged(object sender, EventArgs e)
        {
            var selectedColumnCount = dataGridLocation.Rows
               .GetRowCount(DataGridViewElementStates.Selected);
            if (selectedColumnCount > 0)
            {
                for (var i = 0; i < selectedColumnCount; i++)
                {
                    var selectrows = dataGridLocation.SelectedRows[i];

                    _form1.IdLocations = int.Parse(selectrows.Cells["Id"].Value.ToString());
                    Close();
                }
            }
        }
    }
}
