﻿namespace VDirectAccessWraperDemo
{
    partial class AddUser
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.UserNameLabel = new System.Windows.Forms.Label();
            this.UserFName = new System.Windows.Forms.TextBox();
            this.SaveUser = new System.Windows.Forms.Button();
            this.UserLName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.UserEmail = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.UserCPhone = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.UserPhone = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // UserNameLabel
            // 
            this.UserNameLabel.AutoSize = true;
            this.UserNameLabel.Location = new System.Drawing.Point(12, 22);
            this.UserNameLabel.Name = "UserNameLabel";
            this.UserNameLabel.Size = new System.Drawing.Size(54, 13);
            this.UserNameLabel.TabIndex = 0;
            this.UserNameLabel.Text = "Full Name";
            // 
            // UserFName
            // 
            this.UserFName.Location = new System.Drawing.Point(79, 22);
            this.UserFName.Name = "UserFName";
            this.UserFName.Size = new System.Drawing.Size(193, 20);
            this.UserFName.TabIndex = 1;
            // 
            // SaveUser
            // 
            this.SaveUser.Location = new System.Drawing.Point(79, 215);
            this.SaveUser.Name = "SaveUser";
            this.SaveUser.Size = new System.Drawing.Size(75, 23);
            this.SaveUser.TabIndex = 2;
            this.SaveUser.Text = "Save";
            this.SaveUser.UseVisualStyleBackColor = true;
            this.SaveUser.Click += new System.EventHandler(this.SaveUser_Click);
            // 
            // UserLName
            // 
            this.UserLName.Location = new System.Drawing.Point(79, 48);
            this.UserLName.Name = "UserLName";
            this.UserLName.Size = new System.Drawing.Size(193, 20);
            this.UserLName.TabIndex = 4;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(64, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "Login Name";
            // 
            // UserEmail
            // 
            this.UserEmail.Location = new System.Drawing.Point(79, 74);
            this.UserEmail.Name = "UserEmail";
            this.UserEmail.Size = new System.Drawing.Size(193, 20);
            this.UserEmail.TabIndex = 6;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(32, 13);
            this.label2.TabIndex = 5;
            this.label2.Text = "Email";
            // 
            // UserCPhone
            // 
            this.UserCPhone.Location = new System.Drawing.Point(79, 100);
            this.UserCPhone.Name = "UserCPhone";
            this.UserCPhone.Size = new System.Drawing.Size(193, 20);
            this.UserCPhone.TabIndex = 8;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(12, 100);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Cell Phone";
            // 
            // UserPhone
            // 
            this.UserPhone.Location = new System.Drawing.Point(79, 126);
            this.UserPhone.Name = "UserPhone";
            this.UserPhone.Size = new System.Drawing.Size(193, 20);
            this.UserPhone.TabIndex = 10;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 126);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(38, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Phone";
            // 
            // AddUser
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.UserPhone);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.UserCPhone);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.UserEmail);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.UserLName);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.SaveUser);
            this.Controls.Add(this.UserFName);
            this.Controls.Add(this.UserNameLabel);
            this.Name = "AddUser";
            this.Text = "AddUser";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label UserNameLabel;
        private System.Windows.Forms.TextBox UserFName;
        private System.Windows.Forms.Button SaveUser;
        private System.Windows.Forms.TextBox UserLName;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox UserEmail;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox UserCPhone;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox UserPhone;
        private System.Windows.Forms.Label label4;
    }
}