﻿namespace VDirectAccessWraperDemo
{
    partial class AddMerchant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.MerchantName = new System.Windows.Forms.TextBox();
            this.MerchantAdd = new System.Windows.Forms.Button();
            this.dataGridMerchant = new System.Windows.Forms.DataGridView();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMerchant)).BeginInit();
            this.SuspendLayout();
            // 
            // MerchantName
            // 
            this.MerchantName.Location = new System.Drawing.Point(22, 24);
            this.MerchantName.Name = "MerchantName";
            this.MerchantName.Size = new System.Drawing.Size(200, 20);
            this.MerchantName.TabIndex = 0;
            // 
            // MerchantAdd
            // 
            this.MerchantAdd.Location = new System.Drawing.Point(61, 86);
            this.MerchantAdd.Name = "MerchantAdd";
            this.MerchantAdd.Size = new System.Drawing.Size(118, 23);
            this.MerchantAdd.TabIndex = 1;
            this.MerchantAdd.Text = "Save merchant";
            this.MerchantAdd.UseVisualStyleBackColor = true;
            this.MerchantAdd.Click += new System.EventHandler(this.MerchantAdd_Click);
            // 
            // dataGridMerchant
            // 
            this.dataGridMerchant.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridMerchant.Location = new System.Drawing.Point(273, 13);
            this.dataGridMerchant.Name = "dataGridMerchant";
            this.dataGridMerchant.Size = new System.Drawing.Size(409, 210);
            this.dataGridMerchant.TabIndex = 2;
            this.dataGridMerchant.SelectionChanged += new System.EventHandler(this.dataGridMerchant_SelectionChanged);
            // 
            // AddMerchant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(694, 235);
            this.Controls.Add(this.dataGridMerchant);
            this.Controls.Add(this.MerchantAdd);
            this.Controls.Add(this.MerchantName);
            this.Name = "AddMerchant";
            this.Text = "AddMerchant";
            ((System.ComponentModel.ISupportInitialize)(this.dataGridMerchant)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox MerchantName;
        private System.Windows.Forms.Button MerchantAdd;
        private System.Windows.Forms.DataGridView dataGridMerchant;
    }
}