﻿namespace VDirectAccessWraperDemo
{
    partial class Form1
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.TemplateDataGrid = new System.Windows.Forms.DataGridView();
            this.UserName = new System.Windows.Forms.TextBox();
            this.Password = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.DataGridCurrentParameters = new System.Windows.Forms.DataGridView();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Login = new System.Windows.Forms.Button();
            this.panel2 = new System.Windows.Forms.Panel();
            this.CreateTerminal = new System.Windows.Forms.Button();
            this.TeminalTotalMerchants = new System.Windows.Forms.NumericUpDown();
            this.TerminalTemplateId = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.TerminalDescription = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.TerminalOptionalInfo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.TerminalDownloadId = new System.Windows.Forms.TextBox();
            this.UpdateParameters = new System.Windows.Forms.Button();
            this.GetCompanies = new System.Windows.Forms.Button();
            this.GetTerminlParams = new System.Windows.Forms.Button();
            this.AddMerchant = new System.Windows.Forms.Button();
            this.AddLocations = new System.Windows.Forms.Button();
            this.AssignLocationToTpn = new System.Windows.Forms.Button();
            this.AddingMerchant = new System.Windows.Forms.Label();
            this.AddingLocation = new System.Windows.Forms.Label();
            this.AddUser = new System.Windows.Forms.Button();
            this.UserIdLabel = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.TemplateDataGrid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCurrentParameters)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeminalTotalMerchants)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerminalTemplateId)).BeginInit();
            this.SuspendLayout();
            // 
            // TemplateDataGrid
            // 
            this.TemplateDataGrid.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.TemplateDataGrid.Location = new System.Drawing.Point(12, 90);
            this.TemplateDataGrid.Name = "TemplateDataGrid";
            this.TemplateDataGrid.Size = new System.Drawing.Size(1002, 126);
            this.TemplateDataGrid.TabIndex = 0;
            this.TemplateDataGrid.SelectionChanged += new System.EventHandler(this.TemplateDataGrid_SelectionChanged);
            this.TemplateDataGrid.DoubleClick += new System.EventHandler(this.TemplateDataGrid_DoubleClick);
            // 
            // UserName
            // 
            this.UserName.Location = new System.Drawing.Point(13, 16);
            this.UserName.Name = "UserName";
            this.UserName.Size = new System.Drawing.Size(100, 20);
            this.UserName.TabIndex = 1;
            this.UserName.Text = "kirManager";
            // 
            // Password
            // 
            this.Password.Location = new System.Drawing.Point(119, 16);
            this.Password.Name = "Password";
            this.Password.Size = new System.Drawing.Size(119, 20);
            this.Password.TabIndex = 2;
            this.Password.Text = "K123456&";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, -3);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 3;
            this.label1.Text = "User name";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(116, -3);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Password";
            // 
            // DataGridCurrentParameters
            // 
            this.DataGridCurrentParameters.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.DataGridCurrentParameters.Location = new System.Drawing.Point(12, 238);
            this.DataGridCurrentParameters.Name = "DataGridCurrentParameters";
            this.DataGridCurrentParameters.Size = new System.Drawing.Size(1002, 191);
            this.DataGridCurrentParameters.TabIndex = 5;
            this.DataGridCurrentParameters.CellBeginEdit += new System.Windows.Forms.DataGridViewCellCancelEventHandler(this.DataGridCurrentParameters_CellBeginEdit);
            this.DataGridCurrentParameters.CellEndEdit += new System.Windows.Forms.DataGridViewCellEventHandler(this.DataGridCurrentParameters_CellEndEdit);
            this.DataGridCurrentParameters.SelectionChanged += new System.EventHandler(this.DataGridCurrentParameters_SelectionChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(13, 74);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 13);
            this.label3.TabIndex = 6;
            this.label3.Text = "Templates";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(13, 222);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(217, 13);
            this.label4.TabIndex = 7;
            this.label4.Text = "Terminal parameters || companies || terminals ";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Login);
            this.panel1.Controls.Add(this.Password);
            this.panel1.Controls.Add(this.UserName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Location = new System.Drawing.Point(12, 4);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(288, 41);
            this.panel1.TabIndex = 8;
            // 
            // Login
            // 
            this.Login.Location = new System.Drawing.Point(244, 14);
            this.Login.Name = "Login";
            this.Login.Size = new System.Drawing.Size(41, 23);
            this.Login.TabIndex = 5;
            this.Login.Text = "ok";
            this.Login.UseVisualStyleBackColor = true;
            this.Login.Click += new System.EventHandler(this.Login_Click);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.CreateTerminal);
            this.panel2.Controls.Add(this.TeminalTotalMerchants);
            this.panel2.Controls.Add(this.TerminalTemplateId);
            this.panel2.Controls.Add(this.label10);
            this.panel2.Controls.Add(this.TerminalDescription);
            this.panel2.Controls.Add(this.label9);
            this.panel2.Controls.Add(this.TerminalOptionalInfo);
            this.panel2.Controls.Add(this.label8);
            this.panel2.Controls.Add(this.label7);
            this.panel2.Controls.Add(this.label6);
            this.panel2.Controls.Add(this.TerminalDownloadId);
            this.panel2.Location = new System.Drawing.Point(306, 4);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(711, 40);
            this.panel2.TabIndex = 9;
            // 
            // CreateTerminal
            // 
            this.CreateTerminal.Location = new System.Drawing.Point(633, 14);
            this.CreateTerminal.Name = "CreateTerminal";
            this.CreateTerminal.Size = new System.Drawing.Size(75, 23);
            this.CreateTerminal.TabIndex = 12;
            this.CreateTerminal.Text = "Insert";
            this.CreateTerminal.UseVisualStyleBackColor = true;
            this.CreateTerminal.Click += new System.EventHandler(this.CreateTerminal_Click);
            // 
            // TeminalTotalMerchants
            // 
            this.TeminalTotalMerchants.Location = new System.Drawing.Point(251, 17);
            this.TeminalTotalMerchants.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.TeminalTotalMerchants.Name = "TeminalTotalMerchants";
            this.TeminalTotalMerchants.Size = new System.Drawing.Size(120, 20);
            this.TeminalTotalMerchants.TabIndex = 11;
            // 
            // TerminalTemplateId
            // 
            this.TerminalTemplateId.Location = new System.Drawing.Point(125, 17);
            this.TerminalTemplateId.Maximum = new decimal(new int[] {
            100000,
            0,
            0,
            0});
            this.TerminalTemplateId.Name = "TerminalTemplateId";
            this.TerminalTemplateId.Size = new System.Drawing.Size(120, 20);
            this.TerminalTemplateId.TabIndex = 10;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(499, 1);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(60, 13);
            this.label10.TabIndex = 9;
            this.label10.Text = "Description";
            // 
            // TerminalDescription
            // 
            this.TerminalDescription.Location = new System.Drawing.Point(502, 17);
            this.TerminalDescription.Name = "TerminalDescription";
            this.TerminalDescription.Size = new System.Drawing.Size(116, 20);
            this.TerminalDescription.TabIndex = 8;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(377, 1);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 13);
            this.label9.TabIndex = 7;
            this.label9.Text = "OptionalInfo";
            // 
            // TerminalOptionalInfo
            // 
            this.TerminalOptionalInfo.Location = new System.Drawing.Point(380, 17);
            this.TerminalOptionalInfo.Name = "TerminalOptionalInfo";
            this.TerminalOptionalInfo.Size = new System.Drawing.Size(116, 20);
            this.TerminalOptionalInfo.TabIndex = 6;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(255, 1);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(81, 13);
            this.label8.TabIndex = 5;
            this.label8.Text = "TotalMerchants";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(133, 1);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(60, 13);
            this.label7.TabIndex = 3;
            this.label7.Text = "TemplateId";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(3, 1);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 13);
            this.label6.TabIndex = 2;
            this.label6.Text = "DownloadId";
            // 
            // TerminalDownloadId
            // 
            this.TerminalDownloadId.Location = new System.Drawing.Point(3, 17);
            this.TerminalDownloadId.Name = "TerminalDownloadId";
            this.TerminalDownloadId.Size = new System.Drawing.Size(116, 20);
            this.TerminalDownloadId.TabIndex = 0;
            // 
            // UpdateParameters
            // 
            this.UpdateParameters.Location = new System.Drawing.Point(905, 430);
            this.UpdateParameters.Name = "UpdateParameters";
            this.UpdateParameters.Size = new System.Drawing.Size(106, 23);
            this.UpdateParameters.TabIndex = 11;
            this.UpdateParameters.Text = "Insert Parameters";
            this.UpdateParameters.UseVisualStyleBackColor = true;
            this.UpdateParameters.Click += new System.EventHandler(this.UpdateParameters_Click);
            // 
            // GetCompanies
            // 
            this.GetCompanies.Location = new System.Drawing.Point(181, 47);
            this.GetCompanies.Name = "GetCompanies";
            this.GetCompanies.Size = new System.Drawing.Size(119, 23);
            this.GetCompanies.TabIndex = 12;
            this.GetCompanies.Text = "Get Companies";
            this.GetCompanies.UseVisualStyleBackColor = true;
            this.GetCompanies.Click += new System.EventHandler(this.GetCompanies_Click);
            // 
            // GetTerminlParams
            // 
            this.GetTerminlParams.Location = new System.Drawing.Point(897, 47);
            this.GetTerminlParams.Name = "GetTerminlParams";
            this.GetTerminlParams.Size = new System.Drawing.Size(117, 23);
            this.GetTerminlParams.TabIndex = 15;
            this.GetTerminlParams.Text = "Get Terminl Params";
            this.GetTerminlParams.UseVisualStyleBackColor = true;
            this.GetTerminlParams.Click += new System.EventHandler(this.GetTerminlParams_Click);
            // 
            // AddMerchant
            // 
            this.AddMerchant.Location = new System.Drawing.Point(224, 430);
            this.AddMerchant.Name = "AddMerchant";
            this.AddMerchant.Size = new System.Drawing.Size(106, 23);
            this.AddMerchant.TabIndex = 16;
            this.AddMerchant.Text = "Add Merchant";
            this.AddMerchant.UseVisualStyleBackColor = true;
            this.AddMerchant.Click += new System.EventHandler(this.AddMerchant_Click);
            // 
            // AddLocations
            // 
            this.AddLocations.Location = new System.Drawing.Point(403, 430);
            this.AddLocations.Name = "AddLocations";
            this.AddLocations.Size = new System.Drawing.Size(106, 23);
            this.AddLocations.TabIndex = 17;
            this.AddLocations.Text = "Add Location";
            this.AddLocations.UseVisualStyleBackColor = true;
            this.AddLocations.Click += new System.EventHandler(this.AddLocations_Click);
            // 
            // AssignLocationToTpn
            // 
            this.AssignLocationToTpn.Location = new System.Drawing.Point(605, 430);
            this.AssignLocationToTpn.Name = "AssignLocationToTpn";
            this.AssignLocationToTpn.Size = new System.Drawing.Size(121, 23);
            this.AssignLocationToTpn.TabIndex = 18;
            this.AssignLocationToTpn.Text = "Assign location to tpn";
            this.AssignLocationToTpn.UseVisualStyleBackColor = true;
            this.AssignLocationToTpn.Click += new System.EventHandler(this.AssignLocationToTpn_Click);
            // 
            // AddingMerchant
            // 
            this.AddingMerchant.AutoSize = true;
            this.AddingMerchant.Location = new System.Drawing.Point(336, 435);
            this.AddingMerchant.Name = "AddingMerchant";
            this.AddingMerchant.Size = new System.Drawing.Size(61, 13);
            this.AddingMerchant.TabIndex = 19;
            this.AddingMerchant.Text = "MerchantId";
            // 
            // AddingLocation
            // 
            this.AddingLocation.AutoSize = true;
            this.AddingLocation.Location = new System.Drawing.Point(528, 435);
            this.AddingLocation.Name = "AddingLocation";
            this.AddingLocation.Size = new System.Drawing.Size(57, 13);
            this.AddingLocation.TabIndex = 13;
            this.AddingLocation.Text = "LocationId";
            // 
            // AddUser
            // 
            this.AddUser.Location = new System.Drawing.Point(12, 432);
            this.AddUser.Name = "AddUser";
            this.AddUser.Size = new System.Drawing.Size(106, 23);
            this.AddUser.TabIndex = 20;
            this.AddUser.Text = "Add User";
            this.AddUser.UseVisualStyleBackColor = true;
            this.AddUser.Click += new System.EventHandler(this.AddUser_Click);
            // 
            // UserIdLabel
            // 
            this.UserIdLabel.AutoSize = true;
            this.UserIdLabel.Location = new System.Drawing.Point(125, 436);
            this.UserIdLabel.Name = "UserIdLabel";
            this.UserIdLabel.Size = new System.Drawing.Size(38, 13);
            this.UserIdLabel.TabIndex = 21;
            this.UserIdLabel.Text = "UserId";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 459);
            this.Controls.Add(this.UserIdLabel);
            this.Controls.Add(this.AddUser);
            this.Controls.Add(this.AddingLocation);
            this.Controls.Add(this.AddingMerchant);
            this.Controls.Add(this.AssignLocationToTpn);
            this.Controls.Add(this.AddLocations);
            this.Controls.Add(this.AddMerchant);
            this.Controls.Add(this.GetTerminlParams);
            this.Controls.Add(this.GetCompanies);
            this.Controls.Add(this.UpdateParameters);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.DataGridCurrentParameters);
            this.Controls.Add(this.TemplateDataGrid);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.TemplateDataGrid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DataGridCurrentParameters)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.TeminalTotalMerchants)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TerminalTemplateId)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView TemplateDataGrid;
        private System.Windows.Forms.TextBox UserName;
        private System.Windows.Forms.TextBox Password;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.DataGridView DataGridCurrentParameters;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TextBox TerminalDownloadId;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox TerminalOptionalInfo;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox TerminalDescription;
        private System.Windows.Forms.Button CreateTerminal;
        private System.Windows.Forms.NumericUpDown TeminalTotalMerchants;
        private System.Windows.Forms.NumericUpDown TerminalTemplateId;
        private System.Windows.Forms.Button Login;
        private System.Windows.Forms.Button UpdateParameters;
        private System.Windows.Forms.Button GetCompanies;
        private System.Windows.Forms.Button GetTerminlParams;
        private System.Windows.Forms.Button AddMerchant;
        private System.Windows.Forms.Button AddLocations;
        private System.Windows.Forms.Button AssignLocationToTpn;
        private System.Windows.Forms.Label AddingMerchant;
        private System.Windows.Forms.Label AddingLocation;
        private System.Windows.Forms.Button AddUser;
        private System.Windows.Forms.Label UserIdLabel;
    }
}

