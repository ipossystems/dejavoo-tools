using System.Data;

namespace VDirectAccessWrapper
{
    public class TerminalParams
    {
        public Terminal CurentTerminal { get; set; }
        public DataSet Params { get; set; }
    }
}