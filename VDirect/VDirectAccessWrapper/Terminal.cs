﻿namespace VDirectAccessWrapper
{
    public class Terminal
    {
        public string DownloadId { get; set; }
        public int TemplateId { get; set; }
        public int TotalMerchants { get; set; }
        public string OptionalInfo { get; set; }
        public string Description{ get; set; }
    }
}