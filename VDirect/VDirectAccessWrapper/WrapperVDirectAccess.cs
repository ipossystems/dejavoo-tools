﻿using System;
using System.Data;
using System.Runtime.InteropServices;
using System.ServiceModel;
using VDirectAccessWrapper.mmsxVDirectAccess;

namespace VDirectAccessWrapper
{
    public class WrapperVDirectAccess
    {
        public WrapperVDirectAccess(string user,string password,string url=null)
        {
            UserName = user;
            Password = password;
            Wvda = new VDirectAccessSoapClient();

            if (!string.IsNullOrEmpty(url))
                Wvda.Endpoint.Address = new EndpointAddress(url);
        }

        private VDirectAccessSoapClient Wvda { get; }

        /// <summary>
        /// steam password
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// steam login 
        /// </summary>
        public string UserName { get; set; }


        #region CreateTerminal
        /// <summary>
        /// Show terminals 
        /// </summary>
        /// <returns>available terminals</returns>
        public DataSet ShowTemplateList()
        {
            string error = "";
            var result= Wvda.Show_TemplateList(ref error, UserName, Password);
            if (!string.IsNullOrEmpty(error))
            {
                throw  new Exception(error);
            }
            return result;
        }

        /// <summary>
        /// Create new empty terminal
        /// </summary>
        /// <param name="newTerminal">terminal property</param>
        /// <returns>creation flag</returns>
        public bool  InsertEmptyTerminal(Terminal newTerminal)
        {
            var error = "";
            var result = Wvda.Insert_Terminal(ref error, UserName, Password, newTerminal.DownloadId, newTerminal.TemplateId, newTerminal.TotalMerchants, newTerminal.OptionalInfo, newTerminal.Description);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }

        /// <summary>
        ///  show tempalte parameters
        /// </summary>
        /// <returns> table of parameters</returns>
        public DataSet ShowTemplateParams(Terminal terminal)
        {
            var error = "";
            var result = Wvda.Show_TemplateParameters(ref error, UserName, Password,terminal.TemplateId,terminal.TotalMerchants);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }
        /// <summary>
        /// the addition of terminal settings, and remove existing values
        /// </summary>
        /// <param name="terminalParams">new terminal parameters</param>
        /// <returns></returns>
        public bool InsertTerminalParams(TerminalParams terminalParams)
        {
            var error = "";
            var result = Wvda.Insert_TerminalParameters(ref error, UserName, Password,terminalParams.CurentTerminal.DownloadId,terminalParams.CurentTerminal.TemplateId.ToString(),terminalParams.Params);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }

        /// <summary>
        /// notification  of creation 
        /// </summary>
        /// <param name="terminal">terminal</param>
        /// <param name="dltype">download type</param>
        /// <returns>notification flag</returns>
        public bool PushToDataCenter(Terminal terminal,DOWNLOADTYPE dltype)
        {
            var error = "";
            var result = Wvda.Push_ToDataCenter(ref error, UserName, Password, terminal.DownloadId, terminal.TemplateId.ToString(),dltype);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }

        #endregion

        #region EditeTermonal
        /// <summary>
        /// Get Companies
        /// </summary>
        /// <returns></returns>
        public DataSet GetCompany()
        {
            var error = "";
            var result = Wvda.Get_Companies(ref error, UserName, Password);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }
        public DataSet GetTerminals(int companyId)
        {
            var error = "";
            var result = Wvda.Get_Terminals(ref error, UserName, Password, companyId);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }

        /// <summary>
        /// current terminal parameters
        /// </summary>
        /// <param name="terminalParams">current terminal</param>
        /// <returns>table of parameters</returns>
        public DataSet GetTerminalParams(Terminal terminalParams)
        {
            var error = "";
            var result = Wvda.Get_TerminalParameters(ref error, UserName, Password, terminalParams.DownloadId, terminalParams.TemplateId);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }
        /// <summary>
        /// the addition of the terminal settings without removing the existing values
        /// </summary>
        /// <param name="terminalParams">terminal parameters</param>
        /// <returns>edit flag</returns>
        public bool UpdateOnlyTerminalParams(TerminalParams terminalParams)
        {
            var error = "";
            var result = Wvda.Update_TerminalParameters(ref error, UserName, Password, terminalParams.CurentTerminal.DownloadId, terminalParams.CurentTerminal.TemplateId.ToString(), terminalParams.Params);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }
        /// <summary>
        ///  the addition of terminal settings, and remove existing values
        /// </summary>
        /// <param name="terminalParams">terminal parameters</param>
        /// <returns>edit flag</returns>
        public bool UpdateAllTerminalParams(TerminalParams terminalParams)
        {
            var error = "";
            var result = Wvda.Insert_TerminalParameters(ref error, UserName, Password, terminalParams.CurentTerminal.DownloadId, terminalParams.CurentTerminal.TemplateId.ToString(), terminalParams.Params);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }
        #endregion

        #region MerchantAndLocations
        public int MerchantAdd(string name,int userId)
        {
            var error = "";
            var result = Wvda.MerchantAdd(ref error, UserName, Password, name,userId);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }

        public int LocationAdd(Location lc)
        {
            var error = "";
            var result = Wvda.LocationAdd(ref error, UserName, Password, lc);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }

        public DataSet MerchantLocationList(string tpn="")
        {
            var error = "";
            var result = Wvda.MerchantLocationList(ref error, UserName, Password, tpn);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }
        public bool LocationAssignTpn(string dowloadId, int locationId)
        {
            var error = "";
            var result = Wvda.LocationAssignTpn1(ref error, UserName, Password,dowloadId,locationId);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }
        public DataSet MerchantGetForTerminal(string dowloadId)
        {
            var error = "";
            var result = Wvda.Merchant_GetForTerminal(ref error, UserName, Password,dowloadId);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }
        

        #endregion

        #region User

        public int AddUser(MerchantUser user)
        {
            var error = "";
            var result = Wvda.MerchantUserAdd(ref error, UserName, Password, user);
            if (!string.IsNullOrEmpty(error))
            {
                throw new Exception(error);
            }
            return result;
        }
        #endregion
    }
}
